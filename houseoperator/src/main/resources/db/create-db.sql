CREATE TABLE IF NOT EXISTS house (
  houseId  VARCHAR(10) PRIMARY KEY,
  houseAddress VARCHAR(30),
);
commit;