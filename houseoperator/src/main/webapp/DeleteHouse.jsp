<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="application/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="application/javascript" src="DeleteHouse.js"></script>
</head>
<body>
 <h2>Delete House</h2>
   <p>
    	This is the demo for deleting the house with unique address by requesting 
    	http://localhost:8080/houseoperator/webapi/HouseBuilding/deleteHouse/{houseAddress}
  </p>
  <br/>
  <div id="form">
    <p>
    	<a>House address</a><input id="address"/>
    </p>
    <br/>
    <input type="button" value="Submit" id="submitDelete"></input>
    </div>
  
    <div id="resultDeletehouse">
    </div>
</body>
</html>