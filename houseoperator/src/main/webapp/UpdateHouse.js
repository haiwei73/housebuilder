$(document).ready(function() {
	
	$.each($("#roomList > li"),function(obj){
		$(this).find("input[name='roomtype']").keydown(function(e){
			return checkForAscii(e);
		});
		$(this).find("input[name='roomSize']").keydown(function(e){
			return checkForNumber(e);
		});
		$(this).find("input[name='floor']").keydown(function(e){
			return checkForNumber(e);
		});

	});
	
	
	$("#submitUpdate").click(function(){
		var address = $("#address").val();
		var rooms="";
		$.each($("#roomList > li"),function(obj){
			var roomtype = $(this).find("input[name='roomtype']").val();
			var roomSize =$(this).find("input[name='roomSize']").val();
			var floor = $(this).find("input[name='floor']").val();
			rooms = rooms+roomtype+"&"+roomSize+"&"+floor+"_";
		});
		
		var href = "http://localhost:8080/houseoperator/webapi/HouseBuilding/updateHouse/"+address+"/";
		if(rooms.length > 0){
			href= href+rooms;
		}
		$.ajax({
			url : href,
			dataType : 'text',
			cache : false,
			type : 'POST',
			success : function(data){
			    	$("#resultUpdate").html("");
					$("#resultUpdate").append("Result of query : "+data);
			},
			error : function(jqxhr, textStatus, errorThrown){ 
				$("#resultUpdate").html("");
				$("#resultUpdate").append("Problem : "+errorThrown);
			}
		});
	});
	
	
	$("#addRoom").click(function(){
		$("#listItem").clone().appendTo( "#roomList" );
		
	});

});


function checkForAscii(e){
	var code =  e.keyCode || e.which;
	if (code<91 && code>47 || code== 9 || code== 8){
		return true;
	}
	return false;
}

function checkForNumber(event){
	if( (event.keyCode >= 8 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)) {
		return true;
	}
	return false;
}