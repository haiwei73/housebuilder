<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="application/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="application/javascript" src="ReadHouse.js"></script>
</head>
<body>
 <h2>Read House</h2>
   <p>
    	This is the demo for read the house detail with unique address by requesting 
    	http://localhost:8080/houseoperator/webapi/HouseBuilding/deleteHouse/{houseAddress}
    	The return information include house size ,address, rooms, and a map shows size for each floors {floor number:size}. It's format in JSON string. 
  </p>
  <br/>
  <div id="form">
    <p>
    	<a>House address</a><input id="address"/>
    </p>
    <br/>
    <input type="button" value="Submit" id="submitRead"></input>
    </div>
  
    <div id="resultReadhouse">
    </div>
</body>
</html>