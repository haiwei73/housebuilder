$(document).ready(function() {
	$("#submitRead").click(function(){
		var address = $("#address").val();
		var href = "http://localhost:8080/houseoperator/webapi/HouseBuilding/readHouse/"+address;
		console.log(href);
		$.ajax({
			url : href,
			dataType : 'text',
			cache : false,
			type : 'GET',
			success : function(data){
					console.log(data);
			    	$("#resultReadhouses").html("");
					$("#resultReadhouse").append("Result of query : "+data);
			},
			error : function(jqxhr, textStatus, errorThrown){ 
				$("#resultReadhouse").html("");
				$("#resultReadhouse").append("Problem : "+errorThrown);
			}
		});
	});
	
});


function checkForAscii(e){
	var code =  e.keyCode || e.which;
	if (code<91 && code>47 || code== 9 || code== 8){
		return true;
	}
	return false;
}

function checkForNumber(event){
	if( (event.keyCode >= 8 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)) {
		return true;
	}
	return false;
}