<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="application/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script type="application/javascript" src="UpdateHouse.js"></script>
</head>
<body>
	<p>
    	This is the demo for update the house with rooms by requesting 
		http://localhost:8080/houseoperator/webapi/HouseBuilding/updateHouse/{houseAddress}/{roomList}
		{roomList} is consist in order of type,size,floor with separation of "&". And each room is separated by "_".
		 A valid input will be like this : 
		 http://localhost:8080/houseoperator/webapi/HouseBuilding/updateHouse/112 N Chicago/bathroom&12&1_bedroom&50&2
  	</p>
  	<br/>
	<div id="form">
	<p>
    	<a>House address</a><input id="address"/>
    </p>
  	<ul id="roomList">
    		<li id="listItem"><a>room type : </a><input name="roomtype"/> <a> room size: </a><input name="roomSize"/> <a> floor :</a><input name="floor"/></li>
    </ul>
    <input type="button" id="addRoom" value="Add new room"></input>
    <br/>
    <input type="button" value="Submit" id="submitUpdate"></input>
    </div>
    <div id="resultUpdate">
    </div>
</body>
</html>