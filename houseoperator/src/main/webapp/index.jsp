<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="application/javascript" src="http://code.jquery.com/jquery-1.9.1.js"></script>
</head>
<body>

    <h2>House Building API Demo</h2>
    <p><a href="CreateHouse.jsp">Create House</a></p>
    
    <p><a href="UpdateHouse.jsp">Update House</a></p>
    
    <p><a href="ReadHouse.jsp">Read House</a></p>
    
    <p><a href="DeleteHouse.jsp">Delete House</a></p>
    
</body>
</html>
