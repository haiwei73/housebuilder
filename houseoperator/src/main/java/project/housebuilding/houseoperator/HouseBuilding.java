package project.housebuilding.houseoperator;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import project.housebuilding.model.House;
import project.housebuilding.model.Room;
import project.housebuilding.service.HouseBuildingService;

/**
 * 
 * The Rest API where take URL request in order of creating house, read the detail of house, deleting the house, adding rooms to house
 * @author hwang
 *
 */
@Controller
@Path("HouseBuilding")
public class HouseBuilding {

	@Autowired
	HouseBuildingService houseBuildingService;

	@POST
	@Path("/addHouse/{houseAddress}")
	@Produces(MediaType.TEXT_PLAIN)
	public @ResponseBody String addHouse(
			@PathParam("houseAddress") String houseAddress) {
		if (StringUtils.isBlank(houseAddress)||StringUtils.isBlank(houseAddress.trim())) {
			return "The house address cannot be null";
		}
		House house = new House();
		house.setHouseAddress(houseAddress.toLowerCase().trim());
		String result = houseBuildingService.addHouse(house);
		return result;
	}

	@GET
	@Path("/readHouse/{houseAddress}")
	@Produces(MediaType.APPLICATION_JSON)
	public @ResponseBody String readHouse(
			@PathParam("houseAddress") String houseAddress) {
		if (StringUtils.isBlank(houseAddress)||StringUtils.isBlank(houseAddress.trim())) {
			return "The house address cannot be null";
		}
		House house = new House();
		house.setHouseAddress(houseAddress.toLowerCase().trim());
		String result = houseBuildingService.readHouse(house);
		return result;
	}

	@DELETE
	@Path("/deleteHouse/{houseAddress}")
	@Produces(MediaType.APPLICATION_JSON)
	public @ResponseBody String deleteHouse(
			@PathParam("houseAddress") String houseAddress) {
		if (StringUtils.isBlank(houseAddress)||StringUtils.isBlank(houseAddress.trim())) {
			return "The house address cannot be null";
		}
		House house = new House();
		house.setHouseAddress(houseAddress.toLowerCase().trim());

		String result = houseBuildingService.deleteHouse(house);
		return result;
	}

	@POST
	@Path("/updateHouse/{houseAddress}/{rooms}")
	@Produces(MediaType.APPLICATION_JSON)
	public @ResponseBody String addRoom(
			@PathParam("houseAddress") String houseAddress,
			@PathParam("rooms") String rooms) {
		if (StringUtils.isBlank(houseAddress)||StringUtils.isBlank(houseAddress.trim())) {
			return "The house address cannot be null";
		}
		House house = new House();
		house.setHouseAddress(houseAddress.toLowerCase().trim());
		List<Room> roomList = new ArrayList<Room>();
		if (!StringUtils.isBlank(rooms)) {
			String[] roomsArray = rooms.split("_");
			for (String r : roomsArray) {
				if (!StringUtils.isBlank(r)) {
					String[] detail = r.split("&");
					if (detail.length != 3 || StringUtils.isBlank(detail[0])
							|| StringUtils.isBlank(detail[1])
							|| StringUtils.isBlank(detail[2])) {
						return "room detail missing";
					} else if (!StringUtils.isNumericSpace(detail[1])
							|| !StringUtils.isNumericSpace(detail[2])) {
						return "room size and floor has to be numeric";
					}
					Room ro = new Room();
					ro.setHouseAddress(houseAddress);
					ro.setRoomType(detail[0]);
					ro.setRoomSize(Double.parseDouble(detail[1]));
					ro.setFloor(Integer.parseInt(detail[2]));
					roomList.add(ro);

				}
			}
			house.setRooms(roomList);
		}
		if (CollectionUtils.isEmpty(roomList)) {
			return "No room to add";
		}
		String result = houseBuildingService.addRoom(houseAddress, roomList);
		return result;
	}
}
