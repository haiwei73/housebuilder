package project.housebuilding.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * 
 * Persistence manager implement with singleton pattern 
 * @author hwang
 *
 */
public class PersistenceManager {

	private Connection con;
	private static PersistenceManager persistenceManager;

	private PersistenceManager() {
		try {
			Class.forName("org.h2.Driver");
			this.con = DriverManager.getConnection("jdbc:h2:~/housebuilding",
					"test", "");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static PersistenceManager getInstance() {
		if (persistenceManager == null) {
			persistenceManager = new PersistenceManager();
		}
		return persistenceManager;
	}

	public int executeUpdateQuery(String query) {
		Statement stmt;
		try {
			stmt = con.createStatement();
			int i = stmt.executeUpdate(query);
			return i;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public ResultSet executeFetchQuery(String query) {
		Statement stmt;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery(query);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rs;
	}

	public void close() {
		try {
			con.close();
			persistenceManager = null;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
