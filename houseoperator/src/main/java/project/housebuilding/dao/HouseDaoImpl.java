/**
 * 
 */
package project.housebuilding.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import project.housebuilding.model.House;
import project.housebuilding.model.Room;

/**
 * DAO for execute query with create house, update house, delete house , read house
 * @author Hwang
 *
 */
@Repository("HouseDao")
public class HouseDaoImpl implements HouseDao {
	@Override
	public Integer addHouse(String houseAddress) {
		PersistenceManager pm = PersistenceManager.getInstance();
		int i = pm
				.executeUpdateQuery("CREATE TABLE IF NOT EXISTS house (houseId bigint auto_increment,PRIMARY KEY(houseId), houseAddress varchar(50) UNIQUE NOT NULL)");
		System.out.println(i);
		houseAddress = houseAddress.toLowerCase().trim();
		i = pm.executeUpdateQuery("INSERT INTO house ( houseAddress ) VALUES ( '"
				+ houseAddress + "' )");
		System.out.println(i);
		pm.close();
		return i;
	}

	@Override
	public Integer addRooms(String houseAddress, List<Room> rooms)
			throws SQLException {
		PersistenceManager pm = PersistenceManager.getInstance();
		int i = pm
				.executeUpdateQuery("CREATE TABLE IF NOT EXISTS house (houseId bigint auto_increment,PRIMARY KEY(houseId), houseAddress varchar(50) UNIQUE NOT NULL)");
		i = pm.executeUpdateQuery("CREATE TABLE IF NOT EXISTS room (roomId bigint auto_increment,PRIMARY KEY(roomId), houseAddress varchar(50) NOT NULL, roomType varchar(20), roomSize bigint,floor integer)");
		System.out.println(i);
		ResultSet resultset = pm
				.executeFetchQuery("SELECT * FROM house WHERE houseAddress = '"
						+ houseAddress + "'");
		if (resultset == null) {
			i = pm.executeUpdateQuery("INSERT INTO house ( houseAddress ) VALUES ( '"
					+ houseAddress + "' )");
		}
		int count = 0;
		for (Room r : rooms) {
			i = pm.executeUpdateQuery("INSERT INTO room ( houseAddress,roomType,roomSize,floor ) VALUES ( '"
					+ r.getHouseAddress()
					+ "','"
					+ r.getRoomType()
					+ "',"
					+ r.getRoomSize() + "," + r.getFloor() + " )");
			count = count + i;
		}
		System.out.println(count);
		pm.close();
		return count;
	}

	@Override
	public House readHouse(String houseAddress) throws SQLException {
		PersistenceManager pm = PersistenceManager.getInstance();
		int i = pm
				.executeUpdateQuery("CREATE TABLE IF NOT EXISTS house (houseId bigint auto_increment,PRIMARY KEY(houseId), houseAddress varchar(50) UNIQUE NOT NULL)");
		System.out.println(i);
		houseAddress = houseAddress.toLowerCase().trim();
		ResultSet resultset = pm
				.executeFetchQuery("SELECT * FROM house WHERE house.houseAddress = '"
						+ houseAddress + "'");
		if (resultset != null && resultset.next()) {
			House house = new House();
			i = pm.executeUpdateQuery("CREATE TABLE IF NOT EXISTS room (roomId bigint auto_increment,PRIMARY KEY(roomId), houseAddress varchar(50) NOT NULL, roomType varchar(20), roomSize bigint,floor integer)");
			ResultSet resultsetRooms = pm
					.executeFetchQuery("SELECT * FROM room WHERE houseAddress = '"
							+ houseAddress + "'");
			List<Room> roomList = new ArrayList<Room>();
			while (resultsetRooms != null && resultsetRooms.next()) {
				String roomType = resultsetRooms.getString("roomType");
				Double roomSize = resultsetRooms.getDouble("roomSize");
				Integer floor = resultsetRooms.getInt("floor");
				Room room = new Room();
				room.setFloor(floor);
				room.setHouseAddress(houseAddress);
				room.setRoomSize(roomSize);
				room.setRoomType(roomType);
				roomList.add(room);
			}
			house.setHouseAddress(houseAddress);
			house.setRooms(roomList);
			pm.close();

			return house;
		} else {
			pm.close();
			return null;
		}

	}

	@Override
	public Integer deleteHouse(String houseAddress) throws SQLException {
		PersistenceManager pm = PersistenceManager.getInstance();
		int i = pm
				.executeUpdateQuery("CREATE TABLE IF NOT EXISTS house (houseId bigint auto_increment,PRIMARY KEY(houseId), houseAddress varchar(50) UNIQUE NOT NULL)");
		i = pm.executeUpdateQuery("CREATE TABLE IF NOT EXISTS room (roomId bigint auto_increment,PRIMARY KEY(roomId), houseAddress varchar(50) NOT NULL, roomType varchar(20), roomSize bigint,floor integer)");
		System.out.println(i);
		houseAddress = houseAddress.toLowerCase().trim();
		i = pm.executeUpdateQuery("DELETE FROM house where houseAddress = '"
				+ houseAddress + "'");
		i = pm.executeUpdateQuery("DELETE FROM room where houseAddress = '"
				+ houseAddress + "'");
		System.out.println(i);
		pm.close();
		return i;

	}

}
