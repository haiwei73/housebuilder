package project.housebuilding.dao;

import java.sql.SQLException;
import java.util.List;

import project.housebuilding.model.House;
import project.housebuilding.model.Room;

/**
 * Define the interface for the DAO
 * @author hwang
 *
 */
public interface HouseDao {

	
	Integer addHouse(String houseAddress);

	House readHouse(String houseAddress) throws SQLException;

	Integer addRooms(String houseAddress, List<Room> rooms) throws SQLException;

	Integer deleteHouse(String houseAddress) throws SQLException;

}
