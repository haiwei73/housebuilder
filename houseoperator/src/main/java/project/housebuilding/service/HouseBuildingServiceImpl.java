/**
 * 
 */
package project.housebuilding.service;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import project.housebuilding.dao.HouseDao;
import project.housebuilding.model.House;
import project.housebuilding.model.Room;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * Include create house, delete house, add rooms, read house actions and function to calculate dimension of each floor and the hous size
 * @author Hwang
 *
 */

@Service
public class HouseBuildingServiceImpl implements HouseBuildingService {

	@Autowired
	HouseDao houseDao;

	@Override
	public String addHouse(House house) {
		Integer rowCount = houseDao.addHouse(house.getHouseAddress());
		if (rowCount == 0) {
			return "0 record has been persist";
		} else {
			Gson gson = new GsonBuilder().create();
			String houseJson = gson.toJson(house);
			return rowCount + " records has been persist " + houseJson;
		}

	}

	@Override
	public String addRoom(String houseAddress, List<Room> rooms) {
		Integer rowCount = 0;
		try {
			rowCount = houseDao.addRooms(houseAddress, rooms);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (rowCount == 0) {
			return "0 record has been persist";
		} else {
			Gson gson = new GsonBuilder().create();
			String roomJson = gson.toJson(rooms);
			return rowCount + " records has been persist " + roomJson;
		}

	}

	@Override
	public String readHouse(House house) {
		try {
			House h = houseDao.readHouse(house.getHouseAddress());
			if (h != null) {
				calculateHouseSize(h);
				Gson gson = new GsonBuilder().create();
				String houseJson = gson.toJson(h);
				return houseJson;
			} else {
				return "Can't find house";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Can't find house";
	}

	@Override
	public String deleteHouse(House house) {
		try {
			Integer i = houseDao.deleteHouse(house.getHouseAddress());
			if (i != 0) {

				return "House at " + house.getHouseAddress() + " was deleted";
			} else {
				return "Can't find house";
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Can't find house";
	}

	private void calculateHouseSize(House h) {
		if (h != null) {
			Map<Integer, Double> map = new HashMap<Integer, Double>();
			List<Room> roomList = h.getRooms();
			Double houseSize = 0.0;
			if (roomList != null) {
				for (Room r : roomList) {
					houseSize = houseSize + r.getRoomSize();
					if (map.containsKey(r.getFloor())) {
						Double size = map.get(r.getFloor());
						map.put(r.getFloor(), size + r.getRoomSize());
					} else {
						map.put(r.getFloor(), r.getRoomSize());
					}
				}
				h.setFloorSizeMap(map);
				h.setHouseSize(houseSize);
				h.setNumersOfRooms(roomList.size());
			}
		}
	}
}
