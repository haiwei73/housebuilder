package project.housebuilding.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import project.housebuilding.dao.HouseDao;
import project.housebuilding.model.House;
import project.housebuilding.model.Room;

/**
 * 
 * Interface of house building services
 * @author hwang
 *
 */
public interface HouseBuildingService {

	String addHouse(House house);

	String readHouse(House house);

	String addRoom(String houseAddress, List<Room> rooms);

	String deleteHouse(House house);

}
