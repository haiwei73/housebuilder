package project.housebuilding.model;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * House object 
 * @author hwang
 *
 */
public class House {
	private String houseAddress;
	private int numersOfRooms;
	private double houseSize;
	private List<Room> rooms;
	private BigInteger houseId;
	private Map<Integer, Double> floorSizeMap;

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public double getHouseSize() {
		return houseSize;
	}

	public void setHouseSize(double houseSize) {
		this.houseSize = houseSize;
	}

	public int getNumersOfRooms() {
		return numersOfRooms;
	}

	public void setNumersOfRooms(int numersOfRooms) {
		this.numersOfRooms = numersOfRooms;
	}

	public String getHouseAddress() {
		return houseAddress;
	}

	public void setHouseAddress(String houseAddress) {
		this.houseAddress = houseAddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((floorSizeMap == null) ? 0 : floorSizeMap.hashCode());
		result = prime * result
				+ ((houseAddress == null) ? 0 : houseAddress.hashCode());
		result = prime * result + ((houseId == null) ? 0 : houseId.hashCode());
		long temp;
		temp = Double.doubleToLongBits(houseSize);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + numersOfRooms;
		result = prime * result + ((rooms == null) ? 0 : rooms.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		House other = (House) obj;
		if (floorSizeMap == null) {
			if (other.floorSizeMap != null)
				return false;
		} else if (!floorSizeMap.equals(other.floorSizeMap))
			return false;
		if (houseAddress == null) {
			if (other.houseAddress != null)
				return false;
		} else if (!houseAddress.equals(other.houseAddress))
			return false;
		if (houseId == null) {
			if (other.houseId != null)
				return false;
		} else if (!houseId.equals(other.houseId))
			return false;
		if (Double.doubleToLongBits(houseSize) != Double
				.doubleToLongBits(other.houseSize))
			return false;
		if (numersOfRooms != other.numersOfRooms)
			return false;
		if (rooms == null) {
			if (other.rooms != null)
				return false;
		} else if (!rooms.equals(other.rooms))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "House [houseAddress=" + houseAddress + ", numersOfRooms="
				+ numersOfRooms + ", houseSize=" + houseSize + ", rooms="
				+ rooms + ", houseId=" + houseId + ", floorSizeMap="
				+ floorSizeMap + "]";
	}

	public BigInteger getHouseId() {
		return houseId;
	}

	public void setHouseId(BigInteger houseId) {
		this.houseId = houseId;
	}

	public Map<Integer, Double> getFloorSizeMap() {
		return floorSizeMap;
	}

	public void setFloorSizeMap(Map<Integer, Double> floorSizeMap) {
		this.floorSizeMap = floorSizeMap;
	}

}
