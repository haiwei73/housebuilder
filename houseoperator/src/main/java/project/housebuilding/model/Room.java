package project.housebuilding.model;

import java.math.BigInteger;

import javax.persistence.Entity;

import org.springframework.stereotype.Component;

/**
 * 
 * Room object
 * @author hwang
 *
 */
@Component
public class Room {

	private double roomSize;

	private String roomType;

	private String houseAddress;

	private Integer floor;

	private BigInteger roomId;

	public double getRoomSize() {
		return roomSize;
	}

	
	public void setRoomSize(double roomSize) {
		this.roomSize = roomSize;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}

	public String getHouseAddress() {
		return houseAddress;
	}

	public void setHouseAddress(String houseAddress) {
		this.houseAddress = houseAddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((floor == null) ? 0 : floor.hashCode());
		result = prime * result
				+ ((houseAddress == null) ? 0 : houseAddress.hashCode());
		result = prime * result + ((roomId == null) ? 0 : roomId.hashCode());
		long temp;
		temp = Double.doubleToLongBits(roomSize);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result
				+ ((roomType == null) ? 0 : roomType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Room other = (Room) obj;
		if (floor == null) {
			if (other.floor != null)
				return false;
		} else if (!floor.equals(other.floor))
			return false;
		if (houseAddress == null) {
			if (other.houseAddress != null)
				return false;
		} else if (!houseAddress.equals(other.houseAddress))
			return false;
		if (roomId == null) {
			if (other.roomId != null)
				return false;
		} else if (!roomId.equals(other.roomId))
			return false;
		if (Double.doubleToLongBits(roomSize) != Double
				.doubleToLongBits(other.roomSize))
			return false;
		if (roomType == null) {
			if (other.roomType != null)
				return false;
		} else if (!roomType.equals(other.roomType))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Room [roomSize=" + roomSize + ", roomType=" + roomType
				+ ", houseAddress=" + houseAddress + ", floor=" + floor
				+ ", roomId=" + roomId + "]";
	}

	public Integer getFloor() {
		return floor;
	}

	public void setFloor(Integer floor) {
		this.floor = floor;
	}

	public BigInteger getRoomId() {
		return roomId;
	}

	public void setRoomId(BigInteger roomId) {
		this.roomId = roomId;
	}

}
