# README #

Create a REST API that will create, read, updated and delete "Houses".  Each house can be comprised of any number and type of "Rooms".  Feel free to use any of the Spring projects, and please use an in-memory or embedded database for persistence. For extra credit, add functionality to derive the total square footage of each house and each floor within the house based on the dimensions of each room. 

###Quick summary###

I created the Restful service with H2 embedded database, eclipse, Tomcat, Jersey, Jdbc, and Spring. The API has four actions, create house, delete house, update house, read house. House contain id and unique address, Room contains info of types, size, located floor, and house address. The read house action calls function to calculate the dimension of each floor of the house and the total house size. I also created simple JSP/Jquery demo pages for each of the actions.

### Set up ###
 
1.Download the source code and unzip to workspace of eclipse

2.Import as existing Maven project

3.Create server with Tomcat V8.x and JDK 1.7

4.Maven build project and run on server

5.Index page has demo pages for all four actions

Alternate test method

1. Download Postman 

2. Select matching http method and using the patter below

### Restful API Request URI Pattern ###

1. Create house : http://localhost:8080/houseoperator/webapi/HouseBuilding/addHouse/{houseAddress}

2.Delete house :
http://localhost:8080/houseoperator/webapi/HouseBuilding/deleteHouse/{houseAddress}

3.Read house :
http://localhost:8080/houseoperator/webapi/HouseBuilding/readHouse/{houseAddress}

4.Update house:
http://localhost:8080/houseoperator/webapi/HouseBuilding/updateHouse/{houseAddress}/{roomList}

*roomList contruct with _{roomtype}&{roomSize}&{floor}_, and each room delimit by underscore

### Coding consideration ###

For the data source connection, I created a singleton pattern class to handle it. It's simpler and more flexible on the implementation.
If with larger size of models, it would be better to create data source with JPA by setting up persistence file and annotate entity class.